﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Xamarin.Forms;

namespace TestXamarin
{
	public partial class MainPage : ContentPage
	{
		public MainPage()
		{
			InitializeComponent();

            scrollView.Content = new View1();
		}

        private void changeView_Clicked(object sender, EventArgs e)
        {
            //called manually to make sure animation stops
            ((View1)scrollView.Content).StopAnimation();
            scrollView.Content = new View2();

            //for testing purposes
            GC.Collect();
            GC.WaitForPendingFinalizers();
        }

        private void openModal_Clicked(object sender, EventArgs e)
        {
            Navigation.PushModalAsync(new TestPage());
        }
    }
}
