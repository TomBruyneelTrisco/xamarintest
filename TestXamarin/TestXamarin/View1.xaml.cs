﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace TestXamarin
{
	[XamlCompilation(XamlCompilationOptions.Compile)]
	public partial class View1 : ContentView
	{
        private static int id = 0;
        private Animation animation;



        public View1 ()
		{
			InitializeComponent ();
            var myId = id++;

            System.Diagnostics.Debug.WriteLine("Constructor view 1: " + myId);
            animation = new Animation((value) =>
            {
                //System.Diagnostics.Debug.WriteLine("Animation running value: " + value);
            }, 0, 2, Easing.Linear);

            animation.Commit(this, "testAnimation", 250, 4000, Easing.Linear, (arg1, arg2) =>
            {
                System.Diagnostics.Debug.WriteLine(String.Format("Animation finished ({0}): {1}", DateTime.Now.ToLongTimeString(), 0));
            }, () =>
            {
                return true;
            });
		}

        public void StopAnimation()
        {
            this.AbortAnimation("testAnimation");
        }

        ~View1()
        {
            System.Diagnostics.Debug.WriteLine("Destructor view 1");
        }
    }
}